# mnist 

#### 介绍
parse the mnist data to numpy array

读取Mnist数据集，将其转化为numpy数组，方便python调用。

#### 软件架构
Mnist 类提供了两个方法：next_train_batch()和next_test_batch()。这两个方法接受两个参数，一个是batch size, 另一个是布尔值,表示是否需要label为one_hot。两个函数返回指定个数的数据。
其中image shape 为(size, 784), label shape 为(size,)。如果one_hot = True， label shape 自然是(size, 10)。

#### 安装教程

git clone 本仓库即可使用

#### 使用说明

1.  [从这里下载mnist数据集](http://yann.lecun.com/exdb/mnist/)
2.  解压数据集
3.  git clone 本仓库
4.  下面是一个最简单的使用例子:
```
from mnist import Mnist
def test_Mnist():
    #修改下面的file path为你保存minist的文件夹地址
    #例如"/home/zhangsan/downloads/mnist/"
    mnist = Mnist("/path/to/your/mnist/dir/")
    tr_x, tr_y = mnist.next_train_batch(3, True)
    print(tr_x.shape)
    print(tr_y.shape)
    tr_x = tr_x.reshape((-1, 28, 28))
    for x, y in zip(tr_x, tr_y):
        print(y)
        plt.imshow(x,cmap='gray')
        plt.show()
    te_x, te_y = mnist.next_test_batch(3, True)
    print(te_x.shape)
    print(te_y.shape)
    te_x = te_x.reshape((-1, 28, 28))
    for x, y in zip(te_x, te_y):
        print(y)
        plt.imshow(x,camp='gray')
        plt.show()
if __name__ == "__main__":
    test_Mnist()
```
![输入图片说明](https://images.gitee.com/uploads/images/2020/0423/164421_eb475e6e_5526154.png "example.png")
    
